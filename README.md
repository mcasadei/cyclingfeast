# The Cycling Feast

*Road cycling* and *mountain biking* have been steadily growing in popularity in recent years due to the beneficial effects on health and well-being. This has been fueling an increasing 
interest in services for amateur riders, ranging from professional training programs, *"cycling feasts"* as well as travel-agency services and bike and component
manufactures. Getting data to help highlight the trend of "novel recruits" can be fundamental to refine the aforementioned services and provide a better target for business.

Among the others, **Strava** ([http://strava.com/](http://strava.com/)), dubbed as *"The Social Network of Athletes"* is by far the most well-regarded Web and mobile platform adopted by cyclists 
to track their activities. 
Since its start in 2009, both recreational and professional cyclists have been increasingly used the service to upload their activities and connect (or follow)
others. One of the most peculiar features is that of **segment**: users can create public segments, custom road/off-road paths (corresponding 
to roads and trails in the real world) where community members can compete to obtain the best time and become the KOM (King of the Mountain) or QOM (Queen of the Mountain).

Though most of the data is (reasonably) kept strictly private, data related to segment attempts is publicly available (in an anonymized form) via *Strava REST API*, allowing 
in principle to infer trends on the popularity of cycling as a recreational sport among people.
More specifically, *we exploited the API to download* the details of *more than 11.000 hill and mountain climbs* located *in North Italy and in the Alps*: we chose Italy 
because of its "reputation" as a region of cyclists, furthermore we focused just on climbs as they represent both a dream and a fear for any amateur and, among the 
others, one of the reasons for a fan of cycling and/or mountain biking to ride her bike. In the end, we believe that climbs can be a good starting point to depict a representative trend of the whole amateur cycling community.

Along with section details, such as average grade, length, elevation difference and so far, we downloaded also details of any single attempt on these segments: exploiting preliminary
data analysis we are going to highlight trends on novel bike riders over the past few years. 
The *ultimate goal* would be that of leveraging machine learning techniques to *devise models able to predict the trend in road and mountain bike adoption in coming years*.


## Dataset

For a detailed explanation of the dataset please refer to the corresponding page [dataset](dataset/README.md).

## Exploratory Data Analysis

A preliminary analysis of the dataset was performed and the corresponding results plotted in two different images: for a detailed discussion 
please refer to the page on [exploratory data analysis](images/README.md).
