# A Preliminary Data Analysis

In this early analysis of the dataset, we decided to focus on two different measures that can give an idea of
cycling popularity among people, that is: (i) the total distance traveled on the **11058** downloaded segments, (ii) the quantity of efforts
made on each segment. We then analysed the trend of these measures over time, choosing a reference timespan ranging from 2009 to 2017 included.

<a name="plot1"></a>
First, we charted the trend over years of the total distance traveled on the segments: this can be easily devised by taking
for each year the number of efforts times the corresponding distance of the corresponding segments. The resulting figures are shown in the following image.

![total-distance-traveled-over-years.png](total-distance-traveled-over-years.png)

As we can see, the growth has been exponential: in particular, focusing on the most recent years (2015-2017), the number of kilometers traveled
doubled from more than 2 millions to almost 5 millions of kilometer.
Of course, we are aware that this exponential trend can suffer from a *"technological bias"* caused by an increased availability of gps-enabled devices (bike computers, smartphone)
that have made it possible to integrate sport activities with online service such as Strava. In other words, while in 2009 the number of such devices was low, the availability has been with no doubt much larger in 2015-2017, 
leading to a possible *technological bias* in the figures. However, even though the number and the availability of such devices in 2009 may have been quite low,
starting from 2015, we have witnessed a relative large diffusion and availability of Internet-enabled devices, so that we can assume no "technological bias" affecting the data from 2015 on.

<a name="plot2"></a>
The second plot shows the geolocated count of segment efforts in four different years taken as reference:
* 2009, selected as the year of the official kickoff of Strava, so that we going to regard efforts' quantities on this year as a sort of baseline for subsequent years.
* 2013, as we can suppose that the availability of gps enabled sport devices was large enough so as to represent no technological barrier to athletes to access online services such as Strava.
* Finally, 2015 and 2017 represent the very recent past allowing to understand what kind of growth in the amateur cyclist community one can expected over a shorter timespan (2 years).

The results of this analysis are shown in the below image.

![total-efforts-over-years-geolocalized.png](total-efforts-over-years-geolocalized.png)

Geolocalization on the map was possible by taking start latitude and longitude of each segment and plotting a dot in the corresponding point in the map.
For each year, *red points* depict "new" segments, segments added that very same year, in other words segments not available in previous years. On the contrary, *blu points* 
stand for *preexisting segments*, i.e. segments added in previous years.
As a consequence, just by looking at the colors, we can realized that, while the earlier years 2009 and 2013 witnessed a remarkable growth in the number of segments 
added on Strava, in 2015 and 2017 the number of new segments reached a plateau: indeed, looking at 2013 versus 2015 and 2015 versus 2017, it is pretty hard to identify "new" red points.
This effect is with no doubt due also to the fact that once a mountain or hill pass got covered by someone who created a segment and publicly shared it, trivially there is no need 
to create new segments on it. This also tells us that in the space of few years the whole of the north Appennines and the Alps got completely covered on Strava by one or more segments.

The most interesting data, anyway, lies in the size of dots: a larger one denotes a higher numbers of effort made on the represented segment in a given year.
Looking at the evolution depicted in the map it is easy to recognize a strongly growing trend that, we think, can be regarded as a good candidate predictor of 
the trend in the coming years. Indeed, since technology (gps and Internet enabled bike devices) does not represent anymore a barrier for people to join communities like Strava and, consequently, 
cannot be regarded as a bias on the data, we expect the coming years being revelatory for such a trend and, ultimately, a good reason to further investigate and analyse.